FROM debian:stretch
MAINTAINER Andrew Dunham <andrew@du.nham.ca> Alexander Sack <asac129@gmail.com>

# Install build tools
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get upgrade -yy && \
    DEBIAN_FRONTEND=noninteractive apt-get install -yy \
        automake            \
        bison               \
        build-essential     \
        curl                \
        file                \
        flex                \
        git                 \
        libtool             \
        pkg-config          \
        python              \
        texinfo             \
        vim                 \
        wget

# Install musl-cross
RUN mkdir /build-x86_64 &&                                                 \
    cd /build-x86_64 &&                                                    \
    git clone https://github.com/GregorR/musl-cross.git &&          \
    cd musl-cross &&                                                \
    echo 'ARCH=x86_64'                                 >> config.sh && \
    echo 'TRIPLE=x86_64-linux-musl'                    >> config.sh && \
    echo 'WITH_SYSROOT=yes'                            >> config.sh && \
    echo 'GCC_BUILTIN_PREREQS=yes'                  >> config.sh && \
    sed -i -e "s/^MUSL_VERSION=.*\$/MUSL_VERSION=1.1.14/" defs.sh &&  \
    ./build.sh &&                                                   \
    cd / &&                                                         \
    apt-get clean &&                                                \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /build-x86_64

ENV PATH $PATH:/opt/cross/x86_64-linux-musl/bin/
CMD /bin/bash
